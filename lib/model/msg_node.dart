import 'package:base_utils/log/index.dart';

class MsgNode {
  final dynamic _data;

  MsgNode(this._data);

  dynamic get data => _data;

  bool get isEmpty => _data == null;

  bool get isNotEmpty => !isEmpty;

  void setX(String key, value) {
    if (!isJson) {
      // log.w("IMsgNode.setX($key, $value), but $_data is not Map");
      return;
    }
    _data[key] = value;
  }

  ///
  /// 根据 Key 查找
  /// key: name
  /// return name || defValue
  ///
  T? getX<T>(String key, [T? defValue]) {
    assert(key.isNotEmpty);
    if (!isJson) {
      // log.w("IMsgNode.get$T($key, $defValue) $_data is not Map");
      return defValue;
    }
    final value = _data[key] ?? defValue;

    /// assert
    _assertResult(key, value, defValue);
    //
    return value;
  }

  MsgNode? getNode(String key, [MsgNode? defValue]) {
    assert(key.isNotEmpty);
    if (!isJson) {
      return defValue;
    }
    return MsgNode(_data[key]);
  }

  List<T>? getList<T>(String key, [List<T>? defaultValue]) {
    assert(key.isNotEmpty);
    if (!isJson) {
      return defaultValue;
    }
    var data = _data[key];
    var isList = MsgNode.isList(data);
    if (!isList) {
      return defaultValue;
    }
    return (data as List).map((e) => e as T).toList(growable: false);
  }

  String? getStr(String key, [String? defValue]) {
    return getX<String>(key, defValue);
  }

  int? getInt(String key, [int? defValue]) {
    return getX<int>(key, defValue);
  }

  num? getNum(String key, [num? defValue]) {
    return getX<num>(key, defValue);
  }

  double? getDouble(String key, [double? defValue]) {
    return getX<double>(key, defValue);
  }

  bool? getBool(String key, [bool? defValue]) {
    return getX<bool>(key, defValue);
  }

  // ========================================== 列表 ==========================================

  void mergeFiledOnNonnull<T>(T? field, String fieldName) {
    var value = getX<T>(fieldName);
    if (value == null) {
      return;
    }
    field = value;
  }

  ///
  /// 根据索引查找
  ///
  T? getXAt<T>(int index, [T? defValue]) {
    assert(index >= 0);
    if (!isArray) {
      // log.w("IMsgNode.getXAt<$T>($index, $defValue) $_data is not List");
      return defValue;
    }
    if (_data.length <= index) {
      return null;
    }
    var item = _data[index];

    /// assert
    _assertResult(index, item, defValue);
    //
    return item ?? defValue;
  }

  MsgNode? getNodeAt(int index, [MsgNode? defValue]) {
    var item = getXAt<dynamic>(index);
    return MsgNode(item);
  }

  num? getNumAt(int index, [num? defValue]) {
    return getXAt<num>(index, defValue);
  }

  int? getIntAt(int index, [int? defValue]) {
    return getXAt<int>(index, defValue);
  }

  String? getStrAt(int index, [String? defValue]) {
    return getXAt<String>(index, defValue);
  }

  bool? getBoolAt(int index, [bool? defValue]) {
    return getXAt<bool>(index, defValue);
  }

  double? getDoubleAt(int index, [double? defValue]) {
    return getXAt<double>(index, defValue);
  }

  int getSize() {
    if (isArray) {
      return List.from(_data).length;
    }
    return 0;
  }

  List<MsgNode>? getAll() {
    if (!isArray) {
      return null;
    }
    var list = List.from(_data);
    return list.map<MsgNode>((e) => MsgNode(e)).toList(growable: false);
  }

  ///
  /// keyPath: user.name.firstName
  /// return firstName ?? defValue
  ///
  T? getDeepX<T>(String keyPath, [T? defValue]) {
    assert(keyPath.isNotEmpty);
    List<String> keys = keyPath.split(".");
    if (keys.length == 1) {
      return getX<T>(keys[0], defValue);
    }
    //
    dynamic result;
    MsgNode nextNode = this;
    for (int i = 0; i < keys.length; i++) {
      var key = keys[i];
      var data = nextNode.getX<dynamic>(key);
      if (!isRawData(data)) {
        if (i == keys.length - 1) {
          result = data;
        } else {
          nextNode = MsgNode(data);
        }
        continue;
      }
      result = data;
    }

    /// assert
    _assertResult(keyPath, result, defValue);
    //
    return result ?? defValue;
  }

  // =========================== as action =========================
  T? asT<T>() {
    if (_data == null || _data is T) {
      return _data;
    }
    log.w("IMsgNode.asStr $_data is not $T");
    return null;
  }

  String? asStr() {
    return asT<String>();
  }

  num? asNum() {
    return asT<num>();
  }

  int? asInt() {
    return asT<int>();
  }

  double? asDouble() {
    return asT<double>();
  }

  Map<String, dynamic>? asMap() {
    return asT<Map<String, dynamic>>();
  }

  // ============== util func =========

  void _assertResult<T>(dynamic key, dynamic value, T defValue) {
    if (value != null && value is! T) {
      // log.w(
      //     "IMsgNode.get$T($key, $defValue), expect return $T, but return ${value.runtimeType}");
      // 返回默认值？
      // return defValue;
    }
    // assert 不成功 会阻断后续操作
    assert(value == null || value is T);
  }

  bool get isJson => isMap(_data);

  bool get isArray => isList(_data);

  bool get isRaw => isRawData(_data);

  static bool isMap(dynamic data) {
    return data != null &&
        (data is Map ||
            data.runtimeType.toString().contains("_InternalHashMap"));
  }

  static bool isList(dynamic data) {
    return data != null && data is Iterable;
  }

  static bool isRawData(dynamic data) {
    return data is num || data is String || data is bool;
  }

  static T? compareVersionGetIfBiggerThanNodePassIn<T>(
      String key, dynamic newValue, dynamic oldValue) {
    MsgNode newNode = MsgNode(newValue);
    MsgNode oldNode = MsgNode(oldValue);
    final v = newNode.getInt("version") ?? 0;
    final v1 = oldNode.getInt("version") ?? 0;
    if (v <= v1) return null;
    return newNode.getX<T>(key);
  }
}
