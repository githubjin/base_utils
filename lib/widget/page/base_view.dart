import 'package:base_utils/base_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart' show FrameCallback;

import 'ipresenter.dart';
import 'iview.dart';

abstract class BaseView extends StatefulWidget {
  const BaseView({Key? key}) : super(key: key);
}

abstract class BaseViewState<P extends IPresenter, V extends BaseView>
    extends State<V> implements IView {
  late P _presenter;

  P createPresenter();

  @override
  void initState() {
    _presenter = createPresenter();
    _presenter.attachView(this);
    _presenter.onInit();
    initData();
    super.initState();
  }

  P get presenter => _presenter;

  void initData() {}

  @override
  void dispose() {
    _presenter.detachView();
    super.dispose();
  }

  @override
  void unFocus() {
    if (!mounted) return;
    FocusScope.of(context).unfocus();
  }

  @override
  void showLoading([String? message]) {
    // showDialog(
    //   context: context,
    //   barrierColor: Colors.transparent,
    //   builder: (context) => DialogLoading(title: message),
    // );
    throw UnimplementedError();
  }

  @override
  void hideLoading([String? until]) {
    if (until == null) {
      return Navigator.pop(context);
    }
    Navigator.popUntil(context, ModalRoute.withName(until));
  }

  @override
  void toast(String msg, {bool whiteBackground = false}) {
    /// TODO
    throw UnimplementedError();
  }

  @override
  void pop({bool toRoot = false, String? until, dynamic popData}) {
    if (toRoot) {
      Navigator.popUntil(context, ModalRoute.withName('/'));
      return;
    }
    if (until != null) {
      Navigator.popUntil(context, ModalRoute.withName(until));
      return;
    }
    if (popData != null) {
      Navigator.of(context).pop(popData);
      return;
    }
    Navigator.of(context).pop();
  }

  @override
  void messenger(String message) {
    if (message == 'cancel') return;
    // hide first
    ScaffoldMessenger.of(ContextHolder.value).hideCurrentMaterialBanner();
    // show
    ScaffoldMessenger.of(ContextHolder.value).showMaterialBanner(
      MaterialBanner(
        content: Text(message),
        leading: const Icon(Icons.info_outline_rounded),
        backgroundColor: Colors.yellow,
        actions: [
          TextButton(
            child: const Text('Dismiss'),
            onPressed: () => ScaffoldMessenger.of(ContextHolder.value)
                .hideCurrentMaterialBanner(),
          ),
        ],
      ),
    );
  }

  @override
  void addPostFrameCallback(FrameCallback callback) {
    WidgetsBinding.instance.addPostFrameCallback(callback);
  }
}
