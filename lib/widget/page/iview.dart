import 'dart:ui' show FrameCallback;

abstract class IView {
  void toast(String message, {bool whiteBackground = false});
  void showLoading([String? message]);
  void hideLoading([String? until]);
  void messenger(String message);
  void pop({bool toRoot = false, String? until, dynamic popData});
  void unFocus();
  void addPostFrameCallback(FrameCallback callback);
}
