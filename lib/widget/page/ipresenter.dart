
import 'imodel.dart';
import 'iview.dart';

abstract class IPresenter<V extends IView, M extends IModel> {
  void attachView(V view);
  void detachView();
  void dispose();
  void onInit();
  M get model;
}
