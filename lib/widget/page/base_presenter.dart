import 'dart:math';

import 'package:base_utils/log/index.dart';
import 'package:dio/dio.dart';

import 'imodel.dart';
import 'ipresenter.dart';
import 'iview.dart';

abstract class BasePresenter<V extends IView, M extends IModel>
    implements IPresenter<V, M> {
  M? _model;
  V? _view;

  M createModel();

  @override
  void onInit() {}

  @override
  void attachView(V view) {
    _view = view;
    _model = createModel();
  }

  @override
  void detachView() {
    cancelRequest();
    dispose();
    _view = null;
    // model
    if (!keepModelAlive) {
      _model?.dispose();
      _model = null;
    }
  }

  bool get keepModelAlive => false;

  V? get view => _view;

  @override
  M get model => _model!;

  final Map<String, CancelToken> _cancelTokens = {};

  CancelToken tokenOf(String key) {
    if(!_cancelTokens.containsKey(key)) {
      _cancelTokens[key] = CancelToken();
    }
    return _cancelTokens[key]!;
  }

  void cancelRequest() {
    try {
      for (var element in _cancelTokens.values) {
        element.cancel();
      }
    } catch(e) {
      log.e("cancelRequest failed $e");
    } finally {
      _cancelTokens.clear();
    }
  }
}
