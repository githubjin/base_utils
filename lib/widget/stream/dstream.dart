import 'package:rxdart/rxdart.dart';

///
/// 自定义 stream
/// 配合 DStreamBuilder 使用
///
class DStream<T> {
  /// stream controller
  late BehaviorSubject<T> _data;

  /// stream
  ValueStream<T> get data => _data.stream;

  /// value getter
  T? get value {
    if (_data.hasValue) {
      return _data.value;
    }
    return null;
  }

  DStream(T? initialData) {
    if (initialData == null) {
      _data = BehaviorSubject<T>();
    } else {
      _data = BehaviorSubject<T>.seeded(initialData);
    }
  }

  /// 更新操作
  void update(T data) {
    _data.add(data);
  }

  /// 销毁操作
  void dispose() {
    _data.close();
  }
}
