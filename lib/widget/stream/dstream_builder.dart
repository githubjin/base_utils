import 'package:flutter/material.dart';

import 'dstream.dart';

typedef ContentBuilder<T> = Widget Function(BuildContext context, T? data);

///
/// 自定义 StreamBuilder
///
class DSBuilder<T> extends StatelessWidget {
  final DStream<T> stream;
  final T? initialData;
  final ContentBuilder<T> builder;

  const DSBuilder({
    Key? key,
    required this.stream,
    required this.builder,
    this.initialData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<T>(
      initialData: initialData ?? stream.value,
      stream: stream.data,
      builder: (context, snapshot) {
        return builder.call(context, snapshot.data);
      },
    );
  }
}
