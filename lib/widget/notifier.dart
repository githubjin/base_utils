import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

Widget notifierBuilder<T>(
  ValueListenable<T> listenable,
  ValueWidgetBuilder<T> builder, {
  Widget? child,
  Key? key,
}) {
  return ValueListenableBuilder<T>(
    key: key,
    valueListenable: listenable,
    builder: builder,
    child: child,
  );
}

typedef Value4WidgetBuilder<A, B, C, D> = Function(
    BuildContext context, A a, B b, C c, D d, Widget? child);
typedef Value3WidgetBuilder<A, B, C> = Function(
    BuildContext context, A a, B b, C c, Widget? child);

Widget deep4NotifierBuilder<A, B, C, D>(
  ValueListenable<A> one,
  ValueListenable<B> two,
  ValueListenable<C> three,
  ValueListenable<D> four,
  Value4WidgetBuilder<A, B, C, D> builder, {
  Widget? child,
  Key? key,
}) {
  return notifierBuilder<A>(
    one,
    (_, a, __) {
      return notifierBuilder<B>(two, (_, b, __) {
        return notifierBuilder<C>(three, (_, c, __) {
          return notifierBuilder<D>(four, (context, d, __) {
            return builder(context, a, b, c, d, child);
          });
        });
      });
    },
    key: key,
  );
}

Widget deep3NotifierBuilder<A, B, C>(
  ValueListenable<A> one,
  ValueListenable<B> two,
  ValueListenable<C> three,
  Value3WidgetBuilder<A, B, C> builder, {
  Widget? child,
  Key? key,
}) {
  return notifierBuilder<A>(
    one,
    (_, a, __) {
      return notifierBuilder<B>(two, (_, b, __) {
        return notifierBuilder<C>(three, (context, c, __) {
          return builder(context, a, b, c, child);
        });
      });
    },
    key: key,
  );
}
