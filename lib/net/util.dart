import 'dart:io';

import 'package:base_utils/log/index.dart';
import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';

class NetUtil {
  static const int maxConnectionsPerHost = 20;

  static Dio newDio({BaseOptions? options}) {
    final dio = Dio(options);
    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (client) {
      client.badCertificateCallback = (cert, host, port) {
        log.d("createDio DefaultHttpClientAdapter $host $port");
        return true;
      };
      client.maxConnectionsPerHost = maxConnectionsPerHost;
      return client;
    };
    return dio;
  }
}

extension ResponseExt on Response {
  bool get ok => statusCode == HttpStatus.ok;
}

extension DioExt on Dio {
  Response<T> _errorResponse<T>(String path, String msg) {
    return Response<T>(
      requestOptions: RequestOptions(path: path),
      statusCode: -1,
      statusMessage: msg,
    );
  }

  Future<Response<T>> safeGet<T>(
    String path, {
    Map<String, dynamic>? queryParameters,
    Options? options,
    CancelToken? cancelToken,
    ProgressCallback? onReceiveProgress,
    int? timeoutMs,
  }) async {
    var future = get<T>(
      path,
      queryParameters: queryParameters,
      options: options,
      cancelToken: cancelToken,
      onReceiveProgress: onReceiveProgress,
    ).catchError((e) => _errorResponse<T>(path, "$e"));
    if (timeoutMs != null) {
      future = future.timeout(
        Duration(milliseconds: timeoutMs),
        onTimeout: () =>
            _errorResponse<T>(path, "Timeout in $timeoutMs milliseconds"),
      );
    }
    return future;
  }
}
