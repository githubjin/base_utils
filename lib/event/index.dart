import 'package:flutter/material.dart';

///
///
///
class EventBus {
  static Map<String, List<EventBusListener>> events = {};

  static void on(String event, EventBusListener listener) {
    var list = events[event];
    if (list == null) {
      list = [listener];
    } else {
      list.add(listener);
    }
    events[event] = list;
  }

  static void off(String event, EventBusListener listener) {
    var list = events[event];
    if (list == null) {
      return;
    }
    list.remove(listener);
    if (list.isEmpty) {
      events.remove(event);
    }
  }

  static bool emit<T>(String event, T? data) {
    var hasListener = events[event]?.isNotEmpty ?? false;
    events[event]?.forEach((element) => element.onEvent(data));
    return hasListener;
  }

  static bool isEmpty(String event) {
    return (events[event]?.length ?? 0) == 0;
  }
}

///
///
///
abstract class EventBusListener<T> {
  void onEvent(T? data);
}

///
///
///
mixin EventGroupMixin<T extends StatefulWidget, D> on State<T> implements
    EventBusListener<D> {

  static final Map<String, dynamic> _cachedValues = {};

  static void clearBy(String eventKey) {
    _cachedValues.remove(eventKey);
  }

  static void cache(String eventKey, data) {
    _cachedValues[eventKey] = data;
  }

  static dynamic get(String eventKey) {
    return _cachedValues[eventKey];
  }

  bool item_selected = false;

  D get uniqueValue;

  D? get initSelectValue;

  String get eventKey;

  ValueChanged<D?>? get onTap;

  @override
  void initState() {
    final value = get(eventKey);
    if (value == null) {
      item_selected = initSelectValue == uniqueValue;
    } else {
      item_selected = value == uniqueValue;
    }
    EventBus.on(eventKey, this);
    super.initState();
  }

  @override
  void didUpdateWidget(covariant oldWidget) {
    final value = get(eventKey);
    if (value != null) {
      item_selected = value == uniqueValue;
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    EventBus.off(eventKey, this);
    if (EventBus.isEmpty(eventKey)) {
      clearBy(eventKey);
    }
    super.dispose();
  }

  @override
  void onEvent(D? data) {
    bool flag = false;
    flag = data == uniqueValue;
    if (flag == item_selected) {
      return;
    }
    setState(() {
      item_selected = flag;
    });
  }

  void emit([D? value]) {
    EventBus.emit<D>(eventKey, value ?? uniqueValue);
    onTap?.call(value ?? uniqueValue);
    cache(eventKey, value ?? uniqueValue);
  }

  void toggle() {
    if (item_selected) {
      EventBus.emit<D>(eventKey, null);
      onTap?.call(null);
      return;
    }
    emit();
  }
}