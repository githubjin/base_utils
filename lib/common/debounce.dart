// debounce.dart

import 'dart:async';
import 'package:flutter/foundation.dart';

/// 函数防抖
///
/// [func]: 要执行的方法
/// [delay]: 要迟延的时长
Function debounce(
  VoidCallback func, {
  Duration delay = const Duration(milliseconds: 500),
  String key = "debounce",
}) {
  Timer? timer = _debounces[key];
  target() {
    // 先取消
    if (timer?.isActive ?? false) {
      timer?.cancel();
    }
    timer = Timer(delay, () {
      func.call();
      //  清理
      _debounces.remove(key);
    });
    // 更新保存
    _debounces[key] = timer!;
  }

  return target;
}

Map<String, Timer> _debounces = {};
