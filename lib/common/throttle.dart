// throttle.dart

import 'dart:async';

typedef FutureFunc = Future Function();

/// 函数节流
///
/// [func]: 要执行的方法
FutureFunc throttle(Future Function() func,
    [String key = "throttle", int delayMs = 0]) {
  bool enable = _throttleStatus[key] ?? true;
  Future target() async {
    if (enable == true) {
      _throttleStatus[key] = false;
      final result = func();
      if (delayMs > 0) {
        await Future.delayed(Duration(milliseconds: delayMs));
      }
      _throttleStatus[key] = true;
      return result;
    }
  }

  return target;
}

Map<String, bool> _throttleStatus = {};
