import 'package:base_utils/error/index.dart';
import 'package:flutter/widgets.dart';

import 'ienv.dart';

abstract class AppBoot {
  Future<void> initConf(IEnv env);

  Future<void> initData();

  Future<void> initThirdPartServices();

  Widget initView();

  void _onStart(IEnv env) async {
    WidgetsFlutterBinding.ensureInitialized();
    //
    await initConf(env);
    await initData();
    await initThirdPartServices();
    // runApp
    runApp(initView());
  }

  void start(IEnv env) {
    ErrorUtils.catchException(() => _onStart(env), isDebug: env.isDebug);
  }
}
