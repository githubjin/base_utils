import 'dart:core';

///
/// app bean 注册管理
///
class AppInst {
  static final Map<Type, dynamic> _instances = {};
  static final Map<String, dynamic> _instancesByKey = {};

  ///
  /// 依赖注册 bean
  ///
  static T registerBean<T>(T bean) {
    _instances[T] = bean;
    return bean;
  }

  static T? getBean<T>() {
    return _instances[T];
  }

  static void unRegisterBean<T>() {
    _instances.remove(T);
  }

  ///
  /// 依赖 String key 注册 bean
  ///
  static void registerBeanByKey<T>(String key, T bean) {
    _instancesByKey[key] = bean;
  }

  static T? getBeanByKey<T>(String key) {
    return _instancesByKey[key];
  }

  static void unRegisterBeanByKey(String key) {
    _instancesByKey.remove(key);
  }

  ///
  /// clear all
  ///
  static void clear() {
    _instances.clear();
    _instancesByKey.clear();
  }

  /// create bean instance
  static T instance<T>(T Function() builder) {
    var bean = AppInst.getBean<T>();
    if (bean == null) {
      bean = builder();
      AppInst.registerBean<T>(bean!);
    }
    return bean;
  }
}
