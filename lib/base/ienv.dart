import 'dart:io';

import 'package:base_utils/storage/merge.dart';
import 'package:logger/logger.dart';

enum Envs { dev, prod }

abstract class IEnv with Mergeable implements MergeableListener {
  Envs get val => Envs.dev;

  Level logLevel = Level.verbose;

  bool get isProd => val == Envs.prod;

  bool get isDebug => val == Envs.dev;

  @override
  String get boxName => "env_config_${val.name}";

  ///
  /// 初始化
  ///
  Future<void> init() async {
    await openBox();
    setListener(this);
  }

  @override
  void onMerge(Map<String, dynamic> data, String key, dynamic newValue, dynamic oldValue) {}
}
