import 'package:logger/logger.dart';

late final Logger log;

class LogConfig {
  ///
  /// 初始化
  ///
  static void init(
    Level level, {
    LogOutput? output,
    LogFilter? filter,
    LogPrinter? printer,
  }) {
    log = Logger(
      level: level,
      printer: printer ?? PrettyPrinter(lineLength: 60, methodCount: 5),
      output: output,
      filter: filter,
    );
  }
}
