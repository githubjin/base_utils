import 'dart:io';

import 'package:base_utils/log/index.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as path;

abstract class StorageEntry {
  static const String _boxSubDir = "default_box";

  Future<void> init({String boxSubDir = _boxSubDir}) async {
    await Hive.initFlutter(boxSubDir);
    registerAdapters();
    await prepareBoxes();
  }

  void registerAdapters();

  Future<void> prepareBoxes();
}

class Storage {
  // Hive supports all primitive types, List, Map, DateTime, BigInt and Uint8List
  static Future<bool> save<T>(String boxName,
      String key,
      T data, {
        String boxSuffix = "",
      }) async {
    String _boxName = "$boxName$boxSuffix";
    try {
      var isOpen = Hive.isBoxOpen(_boxName);
      if (isOpen) {
        Hive.box<T>(_boxName).put(key, data);
      } else {
        var box = await Hive.openBox<T>(_boxName);
        box.put(key, data);
      }
    } catch (e) {
      log.w("StorageUtil.save $key error: $e}");
      return false;
    }
    return true;
  }

  static Future<bool> saveLazy<T>(String boxName,
      String key,
      T data, {
        String boxSuffix = "",
      }) async {
    String _boxName = "$boxName$boxSuffix";
    try {
      var isOpen = Hive.isBoxOpen(_boxName);
      if (isOpen) {
        Hive.lazyBox<T>(_boxName).put(key, data);
      } else {
        var box = await Hive.openLazyBox<T>(_boxName);
        box.put(key, data);
      }
    } catch (e) {
      log.w("StorageUtil.saveLazy $key error: $e}");
      return false;
    }
    return true;
  }

  static Future<bool> addAll<T>(String boxName,
      List<T> data, {
        String boxSuffix = "",
        bool replace = false,
      }) async {
    String _boxName = "$boxName$boxSuffix";
    try {
      var isOpen = Hive.isBoxOpen(_boxName);
      Box<T> box;
      if (isOpen) {
        box = Hive.box<T>(_boxName);
      } else {
        box = await Hive.openBox<T>(_boxName);
      }
      if (replace) {
        await box.clear();
      }
      await box.addAll(data);
    } catch (e) {
      log.w("StorageUtil.addAll error: $e}");
      return false;
    }
    return true;
  }

  static T? getSync<T>(String boxName, String key, {String boxSuffix = ""}) {
    String _boxName = "$boxName$boxSuffix";
    return Hive.box<T>(_boxName).get(key);
  }

  static Future<T?> get<T>(String boxName,
      String key, {
        String boxSuffix = "",
      }) async {
    String _boxName = "$boxName$boxSuffix";
    try {
      var isOpen = Hive.isBoxOpen(_boxName);
      if (isOpen) {
        return Hive.box<T>(_boxName).get(key);
      } else {
        var box = await Hive.openBox<T>(_boxName);
        return box.get(key);
      }
    } catch (e) {
      log.w("StorageUtil.get $key error: $e");
    }
    return null;
  }

  static Future<List<T>?> list<T>(String boxName, {
    String boxSuffix = "",
  }) async {
    String _boxName = "$boxName$boxSuffix";
    try {
      var isOpen = Hive.isBoxOpen(_boxName);
      if (isOpen) {
        return Hive
            .box<T>(_boxName)
            .values
            .toList(growable: false)
            .cast<T>();
      } else {
        var box = await Hive.openBox<T>(_boxName);
        return box.values.toList(growable: false).cast<T>();
      }
    } catch (e) {
      log.w("StorageUtil.list error: $e");
    }
    return null;
  }

  static Future<List<String>> keys<T>(String boxName, {
    String boxSuffix = "",
  }) async {
    String _boxName = "$boxName$boxSuffix";
    try {
      var isOpen = Hive.isBoxOpen(_boxName);
      if (isOpen) {
        return Hive
            .box<T>(_boxName)
            .keys
            .toList(growable: false)
            .cast<String>();
      } else {
        var box = await Hive.openBox<T>(_boxName);
        return box.keys.toList(growable: false).cast<String>();
      }
    } catch (e) {
      log.w("StorageUtil.keys error: $e");
    }
    return [];
  }

  static Future<List<T>> values<T>(String boxName, {
    String boxSuffix = "",
  }) async {
    String _boxName = "$boxName$boxSuffix";
    try {
      var isOpen = Hive.isBoxOpen(_boxName);
      if (isOpen) {
        return Hive
            .box<T>(_boxName)
            .values
            .toList(growable: false)
            .cast<T>();
      } else {
        var box = await Hive.openBox<T>(_boxName);
        return box.values.toList(growable: false).cast<T>();
      }
    } catch (e) {
      log.w("StorageUtil.values error: $e");
    }
    return [];
  }

  static Future<void> deleteAt<T>(String boxName,
      int index, {
        String boxSuffix = "",
      }) async {
    String _boxName = "$boxName$boxSuffix";
    try {
      var isOpen = Hive.isBoxOpen(_boxName);
      if (isOpen) {
        return Hive.box<T>(_boxName).deleteAt(index);
      } else {
        var box = await Hive.openBox<T>(_boxName);
        return box.deleteAt(index);
      }
    } catch (e) {
      log.w("StorageUtil.list error: $e");
    }
  }

  static Future<void> putAt<T>(String boxName,
      int index,
      T data, {
        String boxSuffix = "",
      }) async {
    String _boxName = "$boxName$boxSuffix";
    try {
      var isOpen = Hive.isBoxOpen(_boxName);
      if (isOpen) {
        return Hive.box<T>(_boxName).putAt(index, data);
      } else {
        var box = await Hive.openBox<T>(_boxName);
        return box.putAt(index, data);
      }
    } catch (e) {
      log.w("StorageUtil.list error: $e");
    }
  }

  static Future<int> count<T>(String boxName, {
    String boxSuffix = "",
  }) async {
    String _boxName = "$boxName$boxSuffix";
    try {
      var isOpen = Hive.isBoxOpen(_boxName);
      if (isOpen) {
        return Hive
            .box<T>(_boxName)
            .length;
      } else {
        var box = await Hive.openBox<T>(_boxName);
        return box.length;
      }
    } catch (e) {
      log.w("StorageUtil.count error: $e");
    }
    return 0;
  }

  static Future<void> clear<T>(String boxName) async {
    try {
      var isOpen = Hive.isBoxOpen(boxName);
      Box<T> box;
      if (isOpen) {
        box = Hive.box<T>(boxName);
      } else {
        box = await Hive.openBox<T>(boxName);
      }
      await box.clear();
    } catch (e) {
      log.w("StorageUtil.addAll error: $e}");
    }
  }
}

class SFileUtil {
  static Future<void> deleteBoxFile(String boxSubDir, String boxName) async {
    try {
      var appDir = await getApplicationDocumentsDirectory();
      var boxLock = path.join(appDir.path, boxSubDir, "$boxName.lock");
      var boxHive = path.join(appDir.path, boxSubDir, "$boxName.hive");
      await deleteFile(boxLock);
      await deleteFile(boxHive);
    } catch (e) {
      log.e(e);
    }
  }

  static Future<void> deleteFile(String path) async {
    try {
      var file = File(path);
      var exists = await file.exists();
      if (!exists) {
        return;
      }
      await file.delete();
    } catch (e) {
      log.e(e);
    }
  }

  static Future<void> checkOnOpen<T>(String name) async {
    var opened = Hive.isBoxOpen(name);
    if (opened) {
      return;
    }
    await Hive.openBox<T>(name);
  }
}
