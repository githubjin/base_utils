import 'dart:convert';

import 'package:base_utils/log/index.dart';
import 'package:base_utils/model/msg_node.dart';
import 'package:base_utils/net/util.dart';
import 'package:dio/dio.dart';
import 'package:hive/hive.dart';
import 'package:mutex/mutex.dart';
import 'package:flutter/services.dart';
import 'index.dart';

abstract class MergeableListener {
  void onMerge(Map<String, dynamic> data, String key, dynamic newValue,
      dynamic oldValue);
}

abstract class Mergeable {
  static const String _versionKey = "version";

  MergeableListener? _listener;

  final m = Mutex();

  String get boxName;

  ///
  /// 初始化
  ///
  Future<void> openBox({String? boxNameIn}) async {
    if (Hive.isBoxOpen(boxNameIn ?? boxName)) return;
    await Hive.openBox<dynamic>(boxNameIn ?? boxName);
  }

  ///
  /// 取配置
  ///
  dynamic get(String key) => Storage.getSync<dynamic>(boxName, key);

  ///
  /// 修改配置
  ///
  Future<void> set(String key, value) =>
      Storage.save<dynamic>(boxName, key, value);

  Future<List<String>> get keys => Storage.keys<dynamic>(boxName);

  Future<List<dynamic>> get values => Storage.values<dynamic>(boxName);

  ///
  /// 使用local初始化
  /// 使用 remote 后续更新
  ///
  Future<void> mergeWithoutVersion(Map<String, dynamic> data, {
    bool local = true,
    String? boxNameReplace,
  }) async {
    await m.acquire();
    try {
      var count = await Storage.count<dynamic>(boxName);
      if (local && count > 0) {
        return;
      }
      merge(data, boxNameReplace: boxNameReplace);
    } catch (e) {
      log.e(e, "Env.mergeWithoutVersion");
    } finally {
      m.release();
    }
  }

  ///
  /// 根据 version
  /// 将配置合并%&加载到内存中
  ///
  Future<void> mergeWithVersion(Map<String, dynamic> data,
      {String? boxNameReplace, bool forceMerge = false}) async {
    await m.acquire();
    try {
      if (!forceMerge) {
        // 版本比较
        var newVersion = data[_versionKey] as int;
        await openBox();
        var oldVersion = ((await get(_versionKey)) as int?) ?? 0;
        if (newVersion <= oldVersion) return;
      }
      //
      await merge(data, boxNameReplace: boxNameReplace);
    } catch (e) {
      log.e(e, "Env.mergeWithVersion");
    } finally {
      m.release();
    }
  }

  Future<void> merge(Map<String, dynamic> data, {
    String? boxNameReplace,
    bool notify = false,
  }) async {
    final bname = boxNameReplace ?? boxName;
    for (var entry in data.entries) {
      if (notify && _listener != null) {
        final oldValue = await Storage.get<dynamic>(bname, entry.key);
        if (oldValue != entry.value) {
          _listener?.onMerge(data, entry.key, entry.value, oldValue);
          log.i("$bname 's ${entry.key} $oldValue => ${entry.value}");
        }
      }
      await Storage.save<dynamic>(bname, entry.key, entry.value);
    }
  }

  ///
  /// 加载本地配置
  ///
  Future<bool> loadLocalConfig(String jsonPath, {
    String? boxNameReplace,
    bool forceMerge = false,
    bool cache = true,
  }) async {
    bool success = true;
    try {
      String gameJson = await rootBundle.loadString(jsonPath, cache: cache);
      await mergeWithVersion(Map<String, dynamic>.from(jsonDecode(gameJson)),
          boxNameReplace: boxNameReplace, forceMerge: forceMerge);
    } catch (e) {
      success = true;
      log.e("jsonPath load failed $e");
    }
    return success;
  }

  ///
  /// 加载服务端配置
  ///
  Future<bool> loadRemoteConfig(String url, {String? boxNameReplace}) async {
    bool success = true;
    try {
      Dio dio = NetUtil.newDio();
      var response = await dio.safeGet(url);
      if (!response.ok) {
        log.e("loadRemoteMsgs $url failed ${response.statusMessage}");
      }
      var data = response.data;
      if (!MsgNode.isMap(data)) {
        return false;
      }
      await merge(
        Map<String, dynamic>.from(data),
        boxNameReplace: boxNameReplace,
        notify: true,
      );
    } catch (e) {
      success = false;
      log.e("load $url failed, $e");
    }
    return success;
  }

  void setListener(MergeableListener listener) {
    _listener = listener;
  }

  void clearListener() {
    _listener = null;
  }
}
