import 'package:flutter/material.dart';

class ErrorPage extends StatelessWidget {
  final FlutterErrorDetails flutterErrorDetails;

  const ErrorPage({
    Key? key,
    required this.flutterErrorDetails,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var stack = flutterErrorDetails.stack;
    return Scaffold(
      appBar: AppBar(
        title: const Text("Error Detail"),
      ),
      body: ListView(
        children: [
          Text(flutterErrorDetails.exceptionAsString()),
          const Divider(),
          Text(flutterErrorDetails.toStringShort()),
          const Divider(),
          Text(stack.toString()),
        ],
      ),
    );
  }
}
