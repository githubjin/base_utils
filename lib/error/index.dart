import 'dart:async';
import 'dart:isolate';

import 'package:base_utils/base_utils.dart';
import 'package:base_utils/log/index.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

import 'error_detail.dart';

class ErrorUtils {
  static int _tapCount = 1;

  //
  static void init() {
    _initWidgetErrorHandler();
  }

  ///异常处理
  static void catchException<T>(
    T Function() callback, {
    FlutterExceptionHandler? handler, //异常捕捉，用于自定义打印异常
    String? filterRegExp, //异常上报过滤正则，针对message
    bool isDebug = false,
  }) {
    Isolate.current.addErrorListener(RawReceivePort((dynamic pair) async {
      var isolateError = pair as List<dynamic>;
      var _error = isolateError.first;
      var _stackTrace = isolateError.last;
      Zone.current.handleUncaughtError(_error, _stackTrace);
    }).sendPort);
    // This creates a [Zone] that contains the Flutter application and stablishes
    // an error handler that captures errors and reports them.
    //
    // Using a zone makes sure that as many errors as possible are captured,
    // including those thrown from [Timer]s, microtasks, I/O, and those forwarded
    // from the `FlutterError` handler.
    //
    // More about zones:
    //
    // - https://api.dartlang.org/stable/1.24.2/dart-async/Zone-class.html
    // - https://www.dartlang.org/articles/libraries/zones
    runZonedGuarded<Future<void>>(() async {
      callback();
    }, (error, stackTrace) {
      _filterAndUploadException(
        isDebug,
        handler,
        filterRegExp,
        FlutterErrorDetails(exception: error, stack: stackTrace),
      );
    });
    // This captures errors reported by the Flutter framework.
    FlutterError.onError = (details) {
      if (details.stack != null) {
        Zone.current.handleUncaughtError(details.exception, details.stack!);
      } else {
        FlutterError.presentError(details);
      }
    };
  }

  static void _initWidgetErrorHandler() {
    ErrorWidget.builder = (FlutterErrorDetails flutterErrorDetails) {
      /// 上报错误信息
      uploadException(
          message: flutterErrorDetails.toStringShort(),
          detail: flutterErrorDetails.stack?.toString() ?? "",
          data: {"name": flutterErrorDetails.summary.name ?? ""});

      void _viewDetail() {
        _tapCount += 1;
        if (_tapCount % 10 == 0) {
          Navigator.push(
            ContextHolder.value,
            MaterialPageRoute(
              builder: (_) =>
                  ErrorPage(flutterErrorDetails: flutterErrorDetails),
            ),
          );
        }
      }

      ///
      return Material(
        child: GestureDetector(
          onTap: _viewDetail,
          child: Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: const [
                Text("Oops~~"),
                SizedBox(height: 10),
                Icon(Icons.error_outline_rounded),
              ],
            ),
          ),
        ),
      );
    };
  }

  static void _filterAndUploadException(
    _isDebug,
    handler,
    filterRegExp,
    FlutterErrorDetails details,
  ) {
    if (!_filterException(_isDebug, handler, filterRegExp, details)) {
      uploadException(
        message: details.exception.toString(),
        detail: details.stack.toString(),
      );
    }
  }

  static bool _filterException(
    bool _isDebug,
    FlutterExceptionHandler? handler,
    String? filterRegExp,
    FlutterErrorDetails details,
  ) {
    //默认debug下打印异常，不上传异常
    if (_isDebug) {
      handler == null
          ? FlutterError.dumpErrorToConsole(details)
          : handler(details);
      return true;
    }
    //异常过滤
    if (filterRegExp != null) {
      RegExp reg = RegExp(filterRegExp);
      Iterable<Match> matches = reg.allMatches(details.exception.toString());
      if (matches.isNotEmpty) {
        return true;
      }
    }
    return false;
  }

  static void uploadException({
    required String message,
    required String detail,
    Map? data,
  }) {
    //
    log.e("$message \n $detail \n $data");
  }
}
