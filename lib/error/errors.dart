///
/// 文字类警示错误
///
class AlertError extends Error {
  final String message;

  AlertError(this.message);

  @override
  String toString() => "AlertError: $message";
}