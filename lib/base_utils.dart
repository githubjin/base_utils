library base_utils;

import 'package:flutter/material.dart';

class ContextHolder {
  static GlobalKey<NavigatorState> navigatorKey =
  GlobalKey<NavigatorState>(debugLabel: "navigatorKey");

  static BuildContext get value {
    assert(navigatorKey.currentContext != null,
    "Assignment navigatorKey to MaterialApp.navigatorKey first");
    return navigatorKey.currentContext!;
  }
}

class BaseUtils {
  static Locale? convertLocaleStr(String? localeStr) {
    if (localeStr == null) {
      return null;
    }
    var lists = localeStr.split("_");
    if (lists.length == 2) {
      return Locale(lists.first, lists.last);
    }
    if (lists.length == 3) {
      return Locale.fromSubtags(
          languageCode: lists.first,
          scriptCode: lists[1],
          countryCode: lists.last);
    }
    return Locale(lists.first);
  }
}
